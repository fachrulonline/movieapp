package com.fachrulonline.movieapp;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MovieTest {

    @Test
    public void movieValidator_NullGenre(Movie movie) {
        assertTrue(movie.getGenres().size() > 0);
    }

}
