package com.fachrulonline.movieapp;

public interface OnGetMovieCallback {

    void onSuccess(Movie movie);

    void onError();

}
