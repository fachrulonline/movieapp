package com.fachrulonline.movieapp;

public interface OnMoviesClickCallback {
    void onClick(Movie movie);
}
